<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

/** Publication Routes */
Route::group(['prefix' => 'publications'],function(){
    Route::get('/','PublicationController@fetchData')->name('publications');
    Route::post('/create','PublicationController@create')->name('publicationCreate');
    Route::put('/update/{publication}','PublicationController@update')->name('publicationUpdate');
    Route::delete('/delete/{publication}','PublicationController@destroy')->name('publicationsDelete');
});

/** Journalists Routes */
Route::group(['prefix' => 'journalists'],function(){
    Route::get('/{pubId?}','JournalistController@fetchData')->name('journalists');
    Route::post('/create','JournalistController@create')->name('journalistCreate');
    Route::put('/update/{journalist}','JournalistController@update')->name('journalistsUpdate');
    Route::delete('/delete/{journalist}','JournalistController@destroy')->name('journalistsDelete');
});

/** Articles Routes */
Route::group(['prefix' => 'articles'],function(){
    Route::get('/{jouId?}','ArticleController@fetchData')->name('articles');
    Route::post('/create','ArticleController@create')->name('articleCreate');
    Route::put('/update/{article}','ArticleController@update')->name('articleUpdate');
    Route::delete('/delete/{article}','ArticleController@destroy')->name('articleDelete');
});
