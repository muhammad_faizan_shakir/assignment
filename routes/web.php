<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicationController@index');
Route::get('/journalist/{pubId?}', 'JournalistController@index')->name('journalists');
Route::get('/article/{jouId?}', 'ArticleController@index')->name('articles');;

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
