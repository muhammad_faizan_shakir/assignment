<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PublicationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=>'',
            "website"=>'',
            "address_1"=>'',
            "address_2"=>'',
            "city"=>'',
            "state"=>'',
            "zip_code"=>'',
            "country"=>'',
            "media_market"=>'',
        ];
    }
}
