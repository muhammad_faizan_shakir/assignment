<?php

namespace App\Http\Controllers;

use App\Http\Requests\PublicationStoreRequest;
use App\Publication;
use Illuminate\Http\Request;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('publication');
    }

    public function fetchData(){
        try {
            $data = Publication::all();
            return $this->apiSuccessResponse($data);
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    /**
     * @param PublicationStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(PublicationStoreRequest $request)
    {

        try {
            $data = $request->validated();
            $newData =  Publication::create($data);
            return $this->apiSuccessResponse($newData);
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    public function update(PublicationStoreRequest $request, Publication $publication)
    {
        try {
            $data = $request->validated();
            $newData =  $publication->update($data);
            return $this->apiSuccessResponse($newData);
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publication $publication)
    {
        try {
            $publication->delete();
            return $this->apiSuccessResponse();
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
