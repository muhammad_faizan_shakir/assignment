<?php

namespace App\Http\Controllers;

use App\Http\Requests\JournalistStoreRequest;
use App\Journalist;
use Illuminate\Http\Request;

class JournalistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $pubId
     * @return \Illuminate\Http\Response
     */
    public function index($pubId = 0)
    {
        return view('journalist',compact('pubId'));
    }


    /**
     * @param int $pubId
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchData($pubId=0){
        try {
            $data = Journalist::with("publication");
            if($pubId){
               $data =  $data->where('publication_id',$pubId);
            }
            return $this->apiSuccessResponse($data->get());
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param JournalistStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(JournalistStoreRequest $request)
    {
        try {
            $data = $request->validated();
            $newData =  Journalist::create($data);
            return $this->apiSuccessResponse($newData);
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  JournalistStoreRequest  $request
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(JournalistStoreRequest $request, Journalist $journalist)
    {
        try {
            $data = $request->validated();
            $newData =  $journalist->update($data);
            return $this->apiSuccessResponse($newData);
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Journalist $journalist)
    {
        try {
            $journalist->delete();
            return $this->apiSuccessResponse();
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
