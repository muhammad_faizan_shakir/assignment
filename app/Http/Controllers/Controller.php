<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiSuccessResponse($data=null){
        return response()->json(['message'=>'Operation Successful','result'=>$data],'200');
    }

    /**
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiErrorResponse($data=null, $code=422){
        return response()->json(["message"=>"Something Went Wrong", 'data'=>$data],$code);
    }
}
