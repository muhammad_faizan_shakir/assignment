<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\ArticleStoreRequest;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $jourId
     * @return \Illuminate\Http\Response
     */
    public function index($jouId = 0)
    {
        return view('article',compact('jouId'));
    }

    /**
     * @param int $jouId
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchData($jouId=0){
        try {
            $data = Article::with("journalist.publication");
            if($jouId){
                $data =  $data->where('journalist_id',$jouId);
            }
            return $this->apiSuccessResponse($data->get());
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ArticleStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(ArticleStoreRequest $request)
    {
        try {
            $data = $request->validated();
            $newData =  Article::create($data);
            return $this->apiSuccessResponse($newData);
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ArticleStoreRequest $request, Article $article)
    {
        try {
            $data = $request->validated();
            $newData =  $article->update($data);
            return $this->apiSuccessResponse($newData);
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Article $article)
    {
        try {
            $article->delete();
            return $this->apiSuccessResponse();
        }catch (\Exception $exception){
            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
