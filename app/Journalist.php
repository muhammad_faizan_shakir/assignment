<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journalist extends Model
{
    protected $guarded=[];
    public function articles(){
        return $this->hasMany(Article::class);
    }

    public function publication(){
        return $this->belongsTo(Publication::class);
    }

    public function delete()
    {
        /** IT Will Delete The Articles Related To Journalist than delete the record */
        $this->articles()->delete();
        return parent::delete();
    }
}
