<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $guarded=[];

    public function journalists(){
        return $this->hasMany(Journalist::class);
    }
    public function articles(){
        return $this->hasManyThrough(Article::class,Journalist::class);
    }
    public function delete()
    {
        /** IT Will Delete The Journalist Related To Publication then delete the record */
        $this->journalists()->delete();
        return parent::delete();
    }

}
