<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded=[];
    public function journalist(){
        return $this->belongsTo(Journalist::class);
    }
}
