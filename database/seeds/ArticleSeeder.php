<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Article::truncate();
        for ($i=1;$i<=30;$i++){
            for ($j=1;$j<=2;$j++){
                \App\Article::create([
                    "name"=>"Article {$i}.{$j}",
                    "title"=>"Article{$i}.{$j} Title",
                    "subject"=>"Article{$i}.{$j} Subject",
                    "publish_date"=>\Carbon\Carbon::now()->toDate(),
                    "journalist_id"=>$i,
                ]);
            }
        }
    }
}
