<?php

use Illuminate\Database\Seeder;

class JounrnalistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Journalist::truncate();
        for ($i=1;$i<=10;$i++){
            for ($j=1;$j<=3;$j++){
                \App\Journalist::create([
                    "first_name"=>"Journalist {$i}.{$j}",
                    "last_name"=>"Sr.Journalist{$i}.{$j} Title",
                    "email"=>"journalist_{$i}.{$j}@xyz.com",
                    "phone_number"=>"123234324234",
                    "language_spoken"=>"English,Urdu,Punjabi",
                    "title"=>"Sr.Journalist",
                    "publication_id"=>$i
                ]);
            }
        }
    }
}
