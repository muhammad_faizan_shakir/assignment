<?php

use Illuminate\Database\Seeder;

class PublicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Publication::truncate();
        for ($i=1;$i<=10;$i++){
            \App\Publication::create([
                "name"=>"Publication {$i}",
                "website"=>"www.publication{$i}.com",
                "address_1"=>"Address Publication {$i}",
                "address_2"=>"Address2 Publication {$i}",
                "city"=>"Publication City",
                "state"=>"Publication State",
                "zip_code"=>"Publication ZipCode",
                "country"=>"Publication Country",
                "media_market"=>"Publication Media Market",
            ]);
        }
    }
}
