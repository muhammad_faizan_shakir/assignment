<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(PublicationSeeder::class);
        $this->call(JounrnalistSeeder::class);
        $this->call(ArticleSeeder::class);
    }
}
